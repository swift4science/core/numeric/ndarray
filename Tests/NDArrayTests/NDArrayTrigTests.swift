import Foundation
@testable import NDArray
import XCTest

final class NDArrayTrigTests: XCTestCase {
    func testSineFloat() {
        let a = NDArray<Float>([0.0 as Float, 
                                Float.pi / 6, 
                                0.5 * Float.pi, 
                                (5/6) * Float.pi, 
                                Float.pi, 
                                Float.pi + Float.pi / 6, 
                                1.5 * Float.pi, 
                                Float.pi + (5/6) * Float.pi, 
                                2 * Float.pi])
        let c = NDArray.sin(a)
        let expectedResult: [Float] = [0, 0.5, 1, 0.5, 0, -0.5, -1, -0.5, 0]
        XCTAssertEqual(c.data.value, expectedResult, accuracy: 1e-6)
    }

    static var allTests = [
        ("testSineFloat", testSineFloat)
    ]
}
