import Foundation

extension NDArray where Scalar == Float
{
    public static func sin(_ array: NDArray<Float>) -> NDArray<Float> {
        return elementwise(array) { x in Foundation.sin(x) }
    }
}